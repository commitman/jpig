/**
 * Declaring that all non-empty subpackages must have generated package-info.java with
 * {@link tk.labyrinth.jpig.misc4j.codestyle.PackageLevelNonnull @PackageLevelNonnull} on them.
 */
@PackageLevelNonnull
@Propagate(PackageLevelNonnull.class)
package tk.labyrinth.jpig.example.nullability;

import tk.labyrinth.jpig.Propagate;
import tk.labyrinth.jpig.misc4j.codestyle.PackageLevelNonnull;
