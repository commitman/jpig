package tk.labyrinth.jpig.example.nullability.second;

import tk.labyrinth.jpig.example.nullability.first.FirstClass;

public class SecondClass {

	static {
		FirstClass firstClass = new FirstClass();
		//
		if (firstClass.getIntegerFromMap() != null) {
			// This condition must contain redundant null check warning.
		}
	}
}
