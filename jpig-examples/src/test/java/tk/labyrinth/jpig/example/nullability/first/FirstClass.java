package tk.labyrinth.jpig.example.nullability.first;

import tk.labyrinth.jpig.misc4j.codestyle.PackageLevelNonnull;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is a subject of generated {@link PackageLevelNonnull @PackageLevelNonnull} annotation.
 * It contains three nullability warnings that are recognizable by Intellij IDEA. If you don't see them you may want
 * to recompile the class to make annotation processor generate package-info.<br>
 * <br>
 * FIXME: It is currently require full module recompile for AP to work properly.
 */
public class FirstClass {

	@SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
	private final Map<Integer, Integer> integerMap = new HashMap<>();

	/**
	 * This field must contain non-nullability violation warning.
	 */
	@SuppressWarnings("unused")
	private Object nonNullField;

	/**
	 * This field must not contain non-nullability violation warning.
	 */
	@Nullable
	@SuppressWarnings("unused")
	private Object nullableField;

	/**
	 * This method must contain non-nullability violation warning.
	 *
	 * @return nullable?
	 */
	@SuppressWarnings("unused")
	public Integer getInteger() {
		return null;
	}

	public Integer getIntegerFromMap() {
		// TODO: Find out why there is no warning on Map returning nullable value.
		return integerMap.get(null);
	}
}
