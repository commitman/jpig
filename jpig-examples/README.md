## JPIG Example Collection

#### @PackageInfoNonnull

Example of marking fields, method parameters and return values to be non-null by default to be checked by analyzing tools.

It takes single package-info configuration file to work for whole module:

- [Usage](src/test/java/tk/labyrinth/jpig/example/nullability)
- [Annotation](src/test/java/tk/labyrinth/jpig/misc4j/codestyle)

#### JAXB Custom Adapters

TODO

- [Usage](src/test/java/tk/labyrinth/jpig/example/jaxb)

#### Other

If you find any other application for this tool do not hesitate to contact us.

TODO: Specify contacts.
