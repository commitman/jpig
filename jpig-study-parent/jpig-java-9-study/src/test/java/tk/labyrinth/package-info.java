@Nonnull
@Propagate(Nonnull.class)
package tk.labyrinth;

import tk.labyrinth.jpig.Propagate;

import javax.annotation.Nonnull;
