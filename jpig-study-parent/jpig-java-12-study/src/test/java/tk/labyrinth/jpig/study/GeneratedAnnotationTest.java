package tk.labyrinth.jpig.study;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Tests {@link javax.annotation.processing.Generated} exists on Java 9+ compilations.
 */
public class GeneratedAnnotationTest {

	@Test
	void hasGenerated() throws IOException {
		Path packageInfoSourcePath = Paths.get("target/generated-test-sources/test-annotations/tk/labyrinth/jpig/study/package-info.java");
		{
			Assertions.assertTrue(packageInfoSourcePath.toFile().exists());
		}
		String packageInfoSource = IOUtils.toString(packageInfoSourcePath.toUri(), StandardCharsets.UTF_8);
		{
			Assertions.assertTrue(packageInfoSource.contains("import javax.annotation.processing.Generated;"));
			Assertions.assertTrue(packageInfoSource.contains("@Generated(\"tk.labyrinth.jpig.PackageInfoGeneratingProcessor\")"));
		}
	}
}
