package tk.labyrinth.jpig;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jpig.common.CommonInMain;
import tk.labyrinth.jpig.common.CommonInTest;
import tk.labyrinth.jpig.main.MainAnnotation;
import tk.labyrinth.jpig.main.MainInMain;
import tk.labyrinth.jpig.test.TestAnnotation;
import tk.labyrinth.jpig.test.TestInTest;

import java.net.URL;

/**
 * This class tests and demonstrates how colliding package-info behave.<br>
 * Only latest package-info from classpath is read,<br>
 * so if we have multiple package-info we may lose some annotations.<br>
 */
class PackageInfoCollisionTest {

	/**
	 * Only annotations from latest package-info are available.
	 */
	@Test
	void collision() {
		Assertions.assertSame(CommonInTest.class.getPackage(), CommonInMain.class.getPackage());
		Assertions.assertNull(CommonInMain.class.getPackage().getAnnotation(MainAnnotation.class));
		Assertions.assertNotNull(CommonInMain.class.getPackage().getAnnotation(TestAnnotation.class));
		{
			URL commonPackageInfoUrl = CommonInMain.class.getResource("package-info.class");
			Assertions.assertNotNull(commonPackageInfoUrl);
			Assertions.assertTrue(commonPackageInfoUrl.getPath().endsWith("target/test-classes/tk/labyrinth/jpig/common/package-info.class"), commonPackageInfoUrl.getPath());
		}
	}

	@Test
	void noCollision() {
		Assertions.assertNotEquals(MainInMain.class.getPackage(), TestInTest.class.getPackage());
		Assertions.assertNotNull(MainInMain.class.getPackage().getAnnotation(MainAnnotation.class));
		Assertions.assertNotNull(TestInTest.class.getPackage().getAnnotation(TestAnnotation.class));
		{
			URL mainPackageInfoUrl = MainInMain.class.getResource("package-info.class");
			Assertions.assertNotNull(mainPackageInfoUrl);
			Assertions.assertTrue(mainPackageInfoUrl.getPath().endsWith("target/classes/tk/labyrinth/jpig/main/package-info.class"),
					mainPackageInfoUrl.getPath());
		}
		{
			URL testPackageInfoUrl = TestInTest.class.getResource("package-info.class");
			Assertions.assertNotNull(testPackageInfoUrl);
			Assertions.assertTrue(testPackageInfoUrl.getPath().endsWith("target/test-classes/tk/labyrinth/jpig/test/package-info.class"), testPackageInfoUrl.getPath());
		}
	}
}
