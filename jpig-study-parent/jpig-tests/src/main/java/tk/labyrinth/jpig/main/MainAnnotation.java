package tk.labyrinth.jpig.main;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MainAnnotation {
	// empty
}
