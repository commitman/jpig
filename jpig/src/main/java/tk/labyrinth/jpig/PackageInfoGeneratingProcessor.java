package tk.labyrinth.jpig;

import com.google.auto.service.AutoService;
import com.squareup.javapoet.PackageInfoFile;
import com.squareup.javapoet.PackageInfoSpec;
import lombok.extern.slf4j.Slf4j;
import tk.labyrinth.jaap.context.RoundContext;
import tk.labyrinth.jaap.core.CallbackAnnotationProcessor;
import tk.labyrinth.jaap.template.element.ElementTemplate;
import tk.labyrinth.jaap.template.element.PackageElementTemplate;
import tk.labyrinth.jpig.generate.PackageInfoGenerator;
import tk.labyrinth.jpig.generate.PackageInfoGeneratorContext;
import tk.labyrinth.jpig.model.PackageContext;
import tk.labyrinth.jpig.model.PropagationRule;

import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @see Propagate
 */
@AutoService(Processor.class)
@Slf4j
@SupportedAnnotationTypes({
		"tk.labyrinth.jpig.Propagate",
		"tk.labyrinth.jpig.Propagate.Repeater"
})
public class PackageInfoGeneratingProcessor extends CallbackAnnotationProcessor {

	{
		onEachRound(round -> {
			if (!round.getAnnotations().isEmpty()) {
				RoundContext context = RoundContext.of(round);
				List<PackageElementTemplate> packagesWithRules = context.getPackageElements()
						.filter(template -> template.hasMergedAnnotation(Propagate.class))
						.collect(Collectors.toList());
				List<PropagationRule> propagationRules = packagesWithRules.stream().flatMap(PropagationRule::of)
						.collect(Collectors.toList());
				List<PackageContext> packageContexts = collectPackages(context.getTopLevelElements(), propagationRules);
				packageContexts.forEach(packageContext -> {
					PackageInfoSpec packageInfoSpec = PackageInfoGenerator.generate(new PackageInfoGeneratorContext(
							getClass().getCanonicalName(), packageContext, round.getProcessingEnvironment().getSourceVersion()));
					PackageInfoFile packageInfoFile = PackageInfoFile.builder(packageInfoSpec).build();
					try {
						packageInfoFile.writeTo(round.getProcessingEnvironment().getFiler());
					} catch (IOException ex) {
						throw new RuntimeException(ex);
					}
				});
			}
		});
	}

	private PackageContext createContext(List<PropagationRule> propagationRules, String packageQualifiedName) {
		PackageContext result = new PackageContext();
		result.setQualifiedName(packageQualifiedName);
		result.setPropagationRules(propagationRules.stream().filter(propagationRule ->
				propagationRule.matches(packageQualifiedName)).collect(Collectors.toList()));
		return result;
	}

	List<PackageContext> collectPackages(Stream<ElementTemplate> elementTemplates, List<PropagationRule> propagationRules) {
		Map<String, PackageContext> packageContexts = new HashMap<>();
		elementTemplates.forEach(elementTemplate -> {
			if (elementTemplate.isPackageElement()) {
				PackageElementTemplate packageTemplate = elementTemplate.asPackageElement();
				String packageFullyQualifiedName = packageTemplate.getFullyQualifiedName();
				PackageContext context = packageContexts.compute(packageFullyQualifiedName, (key, value) -> {
					PackageContext result;
					if (value != null) {
						if (value.getPackageElementTemplate() != null) {
							throw new IllegalStateException("Duplicate PackageTemplate: " + packageTemplate);
						} else {
							value.setPackageElementTemplate(packageTemplate);
							result = value;
						}
					} else {
						result = createContext(propagationRules, packageFullyQualifiedName);
					}
					if (value != null) {
					}
					return result;
				});
				context.setPackageElementTemplate(packageTemplate);
			} else {
				String packageQualifiedName = elementTemplate.asTypeElement().getPackage().getFullyQualifiedName();
				packageContexts.computeIfAbsent(packageQualifiedName, key ->
						createContext(propagationRules, packageQualifiedName));
			}
		});
		return packageContexts.values().stream()
				.filter(packageContext -> packageContext.getPackageElementTemplate() == null)
				.filter(packageContext -> !packageContext.getPropagationRules().isEmpty())
				.sorted(Comparator.comparing(PackageContext::getQualifiedName))
				.collect(Collectors.toList());
	}
}
