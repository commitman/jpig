package tk.labyrinth.jpig.generate;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.PackageInfoSpec;
import tk.labyrinth.jaap.handle.AnnotationHandle;
import tk.labyrinth.jpig.model.PropagationRule;

import java.util.List;

public class PackageInfoGenerator {

	public static PackageInfoSpec generate(PackageInfoGeneratorContext context) {
		PackageInfoSpec.Builder builder = PackageInfoSpec.builder(context.getPackageContext().getQualifiedName());
		context.getPackageContext().getPropagationRules().stream()
				.map(PropagationRule::getAnnotationHandles)
				.flatMap(List::stream)
				.map(AnnotationHandle::getAnnotationMirror)
				.map(AnnotationSpec::get)
				.forEach(builder::addAnnotation);
		if (context.isSourceVersionNineOrAbove()) {
			// javax.annotation.processing.Generated is added in Java 9.
			builder.addAnnotation(AnnotationSpec.builder(ClassName.get("javax.annotation.processing", "Generated"))
					.addMember("value", CodeBlock.of("$S", context.getGeneratorName()))
					.build());
		}
		return builder.build();
	}
}
