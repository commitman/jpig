package tk.labyrinth.jpig.generate;

import lombok.Value;
import tk.labyrinth.jpig.model.PackageContext;

import javax.lang.model.SourceVersion;

@Value
public class PackageInfoGeneratorContext {

	String generatorName;

	PackageContext packageContext;

	SourceVersion sourceVersion;

	public boolean isSourceVersionNineOrAbove() {
		return sourceVersion.compareTo(SourceVersion.RELEASE_8) > 0;
	}
}
