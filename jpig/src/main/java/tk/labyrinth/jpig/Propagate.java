package tk.labyrinth.jpig;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Target;

/**
 * Instruction to propagate annotations on package-info generation.
 * For annotation to be propagated its type must be specified in "value" parameter.
 * Whether or not annotation must be added to a specific package-info is decided based on "acceptMasks" and
 * "rejectMasks" properties.
 *
 * @see PackageInfoGeneratingProcessor
 */
@Repeatable(Propagate.Repeater.class)
@Target(ElementType.PACKAGE)
public @interface Propagate {

	String[] acceptMasks() default "*";

	String[] rejectMasks() default {};

	Class<? extends Annotation>[] value();

	@Target(ElementType.PACKAGE)
	@interface Repeater {

		Propagate[] value();
	}
}
