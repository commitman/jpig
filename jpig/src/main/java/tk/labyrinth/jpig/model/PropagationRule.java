package tk.labyrinth.jpig.model;

import lombok.Value;
import tk.labyrinth.jaap.annotation.MergedAnnotation;
import tk.labyrinth.jaap.handle.AnnotationHandle;
import tk.labyrinth.jaap.template.AnnotationTemplate;
import tk.labyrinth.jaap.template.element.PackageElementTemplate;
import tk.labyrinth.jpig.Propagate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Value
public class PropagationRule {

	List<AnnotationHandle> annotationHandles;

	String packageName;

	MergedAnnotation propagateAnnotation;

	public boolean matches(String packageQualifiedName) {
		return packageQualifiedName.startsWith(packageName)
				&& propagateAnnotation.get("acceptMasks").getStrings().map(PropagationRule::maskToPattern)
				.anyMatch(packageQualifiedName::matches)
				&& propagateAnnotation.get("rejectMasks").getStrings().map(PropagationRule::maskToPattern)
				.noneMatch(packageQualifiedName::matches);
	}

	private static String maskToPattern(String mask) {
		return mask.replace("?", ".?").replace("*", ".*?");
	}

	public static Stream<PropagationRule> of(PackageElementTemplate packageElementTemplate) {
		List<MergedAnnotation> propagateAnnotations = new ArrayList<>();
		List<AnnotationHandle> notPropagateHandles = new ArrayList<>();
		{
			AnnotationTemplate propagateAnnotationTemplate = packageElementTemplate.getProcessingContext().getAnnotationTemplate(Propagate.class);
			packageElementTemplate.getDirectAnnotations().forEach(annotationHandle -> {
				List<MergedAnnotation> resolvedPropagateAnnotations = annotationHandle.getMergedAnnotations(propagateAnnotationTemplate)
						.collect(Collectors.toList());
				if (!resolvedPropagateAnnotations.isEmpty()) {
					propagateAnnotations.addAll(resolvedPropagateAnnotations);
				} else {
					notPropagateHandles.add(annotationHandle);
				}
			});
		}
		Map<MergedAnnotation, List<AnnotationHandle>> propagationGroups = new HashMap<>();
		{
			propagateAnnotations.forEach(propagateAnnotation -> propagateAnnotation.get("value").getAnnotationTypes()
					.forEach(annotationTemplate -> notPropagateHandles
							.forEach(annotationHandle -> {
								if (Objects.equals(annotationHandle.getTemplate(), annotationTemplate)) {
									propagationGroups.computeIfAbsent(propagateAnnotation,
											key -> new ArrayList<>()).add(annotationHandle);
								}
							})));
		}
		return propagationGroups.entrySet().stream().map(entry -> new PropagationRule(
				entry.getValue(), packageElementTemplate.getFullyQualifiedName(), entry.getKey()));
	}
}
