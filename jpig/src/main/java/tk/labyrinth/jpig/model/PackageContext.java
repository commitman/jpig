package tk.labyrinth.jpig.model;

import lombok.Data;
import tk.labyrinth.jaap.template.element.PackageElementTemplate;

import java.util.ArrayList;
import java.util.List;

@Data
public class PackageContext {

	private PackageElementTemplate packageElementTemplate;

	private List<PropagationRule> propagationRules = new ArrayList<>();

	private String qualifiedName;
}
