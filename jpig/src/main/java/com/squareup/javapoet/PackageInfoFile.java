package com.squareup.javapoet;

import javax.annotation.processing.Filer;
import javax.lang.model.element.Element;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.squareup.javapoet.Util.checkNotNull;

/**
 * Java file containing package-info.<br>
 * Based on {@link JavaFile}.<br>
 */
public class PackageInfoFile {

	private static final Appendable NULL_APPENDABLE = new Appendable() {
		@Override
		public Appendable append(CharSequence charSequence) {
			return this;
		}

		@Override
		public Appendable append(CharSequence charSequence, int start, int end) {
			return this;
		}

		@Override
		public Appendable append(char c) {
			return this;
		}
	};

	public final PackageInfoSpec packageInfoSpec;

	private final String indent;

	private PackageInfoFile(Builder builder) {
		this.indent = builder.indent;
		this.packageInfoSpec = builder.packageInfoSpec;
	}

	private void emit(CodeWriter codeWriter) throws IOException {
		codeWriter.pushPackage(packageInfoSpec.packageName);
//		{
//			if (!fileComment.isEmpty()) {
//				codeWriter.emitComment(fileComment);
//			}
//		}
		{
			packageInfoSpec.emit(codeWriter);
		}
//		{
//			if (!packageName.isEmpty()) {
//				codeWriter.emit("package $L;\n", packageName);
//				codeWriter.emit("\n");
//			}
//		}
//		{
//			int importedTypesCount = 0;
//			for (ClassName className : new TreeSet<>(codeWriter.importedTypes().values())) {
//				if (skipJavaLangImports && className.packageName().equals("java.lang")) continue;
//				codeWriter.emit("import $L;\n", className.withoutAnnotations());
//				importedTypesCount++;
//			}
//			if (importedTypesCount > 0) {
//				codeWriter.emit("\n");
//			}
//		}
		codeWriter.popPackage();
	}

	public Builder toBuilder() {
		Builder builder = new Builder(packageInfoSpec);
		builder.indent = indent;
		return builder;
	}

	@Override
	public String toString() {
		try {
			StringBuilder result = new StringBuilder();
			writeTo(result);
			return result.toString();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public void writeTo(Appendable out) throws IOException {
		// First pass: emit the entire class, just to collect the types we'll need to import.
		CodeWriter importsCollector = new CodeWriter(NULL_APPENDABLE, indent, Collections.emptySet());
		emit(importsCollector);
		Map<String, ClassName> suggestedImports = importsCollector.suggestedImports();
		//
		// Second pass: write the code, taking advantage of the imports.
		CodeWriter codeWriter = new CodeWriter(out, indent, suggestedImports, Collections.emptySet());
		emit(codeWriter);
	}

	public void writeTo(Filer filer) throws IOException {
		String fileName = packageInfoSpec.packageName + ".package-info";
		List<Element> originatingElements = packageInfoSpec.originatingElements;
		//
		JavaFileObject filerSourceFile = filer.createSourceFile(fileName, originatingElements.toArray(new Element[0]));
		//
		boolean succeed = false;
		try (Writer writer = filerSourceFile.openWriter()) {
			writeTo(writer);
			succeed = true;
		} finally {
			if (!succeed) {
				filerSourceFile.delete();
			}
		}
	}

	public static Builder builder(PackageInfoSpec packageInfoSpec) {
		checkNotNull(packageInfoSpec, "packageInfoSpec == null");
		return new Builder(packageInfoSpec);
	}

	/**
	 * Based on {@link JavaFile.Builder}
	 */
	public static final class Builder {

		private final PackageInfoSpec packageInfoSpec;

		private String indent = "  ";

		private Builder(PackageInfoSpec packageInfoSpec) {
			this.packageInfoSpec = packageInfoSpec;
		}

		public PackageInfoFile build() {
			return new PackageInfoFile(this);
		}
	}
}
