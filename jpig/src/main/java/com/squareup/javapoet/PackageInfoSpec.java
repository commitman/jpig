package com.squareup.javapoet;

import javax.lang.model.element.Element;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import static com.squareup.javapoet.Util.checkArgument;
import static com.squareup.javapoet.Util.checkNotNull;

/**
 * Generated package-info declaration.<br>
 * Based on {@link TypeSpec}<br>
 */
public class PackageInfoSpec {

	public final List<AnnotationSpec> annotations;

	public final CodeBlock javadoc;

	public final List<Element> originatingElements;

	public final String packageName;

	private PackageInfoSpec(Builder builder) {
		this.annotations = Util.immutableList(builder.annotations);
		this.javadoc = builder.javadoc.build();
		this.originatingElements = Util.immutableList(builder.originatingElements);
		this.packageName = builder.packageName;
	}

	void emit(CodeWriter codeWriter) throws IOException {
		{
			codeWriter.emitJavadoc(javadoc);
		}
		{
			codeWriter.emitAnnotations(annotations, false);
		}
		{
			codeWriter.emit("package $L;\n", packageName);
		}
		{
			boolean addedLine = false;
			for (ClassName className : new TreeSet<>(codeWriter.importedTypes().values())) {
				// TODO: !skipJavaLangImports
				if (!className.packageName().equals("java.lang")) {
					if (!addedLine) {
						codeWriter.emit("\n");
						addedLine = true;
					}
					codeWriter.emit("import $L;\n", className.withoutAnnotations());
				}
			}
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null) return false;
		if (getClass() != o.getClass()) return false;
		return toString().equals(o.toString());
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	/**
	 * Based on {@link TypeSpec#toBuilder()}
	 *
	 * @return non-null
	 */
	public Builder toBuilder() {
		Builder builder = new Builder(packageName);
		builder.annotations.addAll(annotations);
		builder.javadoc.add(javadoc);
		return builder;
	}

	@Override
	public String toString() {
		StringBuilder out = new StringBuilder();
		try {
			CodeWriter codeWriter = new CodeWriter(out);
			emit(codeWriter);
			return out.toString();
		} catch (IOException e) {
			throw new AssertionError();
		}
	}

	public static Builder builder(String packageName) {
		return new Builder(checkNotNull(packageName, "packageName == null"));
	}

	/**
	 * Based on {@link TypeSpec.Builder}
	 */
	public static final class Builder {

		private final List<AnnotationSpec> annotations = new ArrayList<>();

		private final CodeBlock.Builder javadoc = CodeBlock.builder();

		private final List<Element> originatingElements = new ArrayList<>();

		private final String packageName;

		private Builder(String packageName) {
			this.packageName = packageName;
		}

		/**
		 * Based on {@link TypeSpec.Builder#addAnnotation(AnnotationSpec)}
		 *
		 * @param annotationSpec non-null
		 *
		 * @return non-null
		 */
		public Builder addAnnotation(AnnotationSpec annotationSpec) {
			checkNotNull(annotationSpec, "annotationSpec == null");
			this.annotations.add(annotationSpec);
			return this;
		}

		/**
		 * Based on {@link TypeSpec.Builder#addAnnotation(Class)}
		 *
		 * @param annotation non-null
		 *
		 * @return non-null
		 */
		public Builder addAnnotation(Class<?> annotation) {
			return addAnnotation(ClassName.get(annotation));
		}

		/**
		 * Based on {@link TypeSpec.Builder#addAnnotation(ClassName)}
		 *
		 * @param annotation non-null
		 *
		 * @return non-null
		 */
		public Builder addAnnotation(ClassName annotation) {
			return addAnnotation(AnnotationSpec.builder(annotation).build());
		}

		/**
		 * Based on {@link TypeSpec.Builder#addAnnotations(Iterable)}
		 *
		 * @param annotationSpecs non-null
		 *
		 * @return non-null
		 */
		public Builder addAnnotations(Iterable<AnnotationSpec> annotationSpecs) {
			checkArgument(annotationSpecs != null, "annotationSpecs == null");
			for (AnnotationSpec annotationSpec : annotationSpecs) {
				this.annotations.add(annotationSpec);
			}
			return this;
		}

		/**
		 * Based on {@link TypeSpec.Builder#addJavadoc(CodeBlock)}
		 *
		 * @param block non-null
		 *
		 * @return non-null
		 */
		public Builder addJavadoc(CodeBlock block) {
			javadoc.add(block);
			return this;
		}

		/**
		 * Based on {@link TypeSpec.Builder#addJavadoc(String, Object...)}
		 *
		 * @param format non-null
		 * @param args   non-null
		 *
		 * @return non-null
		 */
		public Builder addJavadoc(String format, Object... args) {
			javadoc.add(format, args);
			return this;
		}

		/**
		 * Based on {@link TypeSpec.Builder#addOriginatingElement(Element)}
		 *
		 * @param originatingElement non-null
		 *
		 * @return non-null
		 */
		public Builder addOriginatingElement(Element originatingElement) {
			originatingElements.add(originatingElement);
			return this;
		}

		public PackageInfoSpec build() {
			return new PackageInfoSpec(this);
		}
	}
}
