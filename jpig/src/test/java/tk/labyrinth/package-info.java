//
@Everywhere
@Propagate(Everywhere.class)
@Propagate( // Propagates 1 annotation to adapter package's children (but not to adapter package itself).
		acceptMasks = "tk.labyrinth.jpig.jaxb.adapter.*",
		value = {XmlJavaTypeAdapter.class}
)
@Propagate( // Propagates 2 annotations to model package and its children.
		acceptMasks = "tk.labyrinth.jpig.jaxb.model*",
		value = {XmlJavaTypeAdapter.class, XmlJavaTypeAdapters.class}
)
@XmlJavaTypeAdapter(InstantJaxbAdapter.class)
@XmlJavaTypeAdapters({
		@XmlJavaTypeAdapter(LocalDateJaxbAdapter.class),
		@XmlJavaTypeAdapter(LocalTimeJaxbAdapter.class)
})
package tk.labyrinth;

import tk.labyrinth.jpig.Propagate;
import tk.labyrinth.jpig.jaxb.adapter.InstantJaxbAdapter;
import tk.labyrinth.jpig.jaxb.adapter.local.LocalDateJaxbAdapter;
import tk.labyrinth.jpig.jaxb.adapter.local.LocalTimeJaxbAdapter;
import tk.labyrinth.jpig.test.Everywhere;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
