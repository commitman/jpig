package tk.labyrinth.jpig.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.jpig.jaxb.adapter.InstantJaxbAdapter;
import tk.labyrinth.jpig.jaxb.adapter.local.LocalDateJaxbAdapter;
import tk.labyrinth.jpig.jaxb.adapter.local.LocalTimeJaxbAdapter;
import tk.labyrinth.jpig.jaxb.model.ModelRoot;
import tk.labyrinth.jpig.jaxb.model.child.ModelChild;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class GeneratedPackageInfoTest {

	@Test
	void adaptersForChildOnly() {
		{
			Package pack = InstantJaxbAdapter.class.getPackage();
			Assertions.assertNotNull(pack);
			Assertions.assertEquals("tk.labyrinth.jpig.jaxb.adapter", pack.getName());
			Assertions.assertNull(pack.getAnnotation(XmlJavaTypeAdapter.class));
			Assertions.assertNull(pack.getAnnotation(XmlJavaTypeAdapters.class));
		}
		{
			Package pack = LocalDateJaxbAdapter.class.getPackage();
			Assertions.assertNotNull(pack);
			Assertions.assertEquals("tk.labyrinth.jpig.jaxb.adapter.local", pack.getName());
			assertNotNullAnd(pack.getAnnotation(XmlJavaTypeAdapter.class), annotation ->
					Assertions.assertEquals(annotation.value(), InstantJaxbAdapter.class));
			Assertions.assertNull(pack.getAnnotation(XmlJavaTypeAdapters.class));
		}
	}

	@Test
	void adaptersForRootAndChild() {
		Consumer<Package> test = pack -> {
			assertNotNullAnd(pack.getAnnotation(XmlJavaTypeAdapter.class), annotation ->
					Assertions.assertEquals(annotation.value(), InstantJaxbAdapter.class));
			assertNotNullAnd(pack.getAnnotation(XmlJavaTypeAdapters.class), annotation -> {
				List<XmlJavaTypeAdapter> adapterAnnotations = Arrays.asList(annotation.value());
				Assertions.assertEquals(2, adapterAnnotations.size());
				Assertions.assertEquals(LocalDateJaxbAdapter.class, adapterAnnotations.get(0).value());
				Assertions.assertEquals(LocalTimeJaxbAdapter.class, adapterAnnotations.get(1).value());
			});
		};
		{
			Package pack = ModelRoot.class.getPackage();
			Assertions.assertNotNull(pack);
			Assertions.assertEquals("tk.labyrinth.jpig.jaxb.model", pack.getName());
			test.accept(pack);
		}
		{
			Package pack = ModelChild.class.getPackage();
			Assertions.assertNotNull(pack);
			Assertions.assertEquals("tk.labyrinth.jpig.jaxb.model.child", pack.getName());
			test.accept(pack);
		}
	}

	@Test
	void everywhere() {
		Assertions.assertNotNull(Everywhere.class.getPackage().getAnnotation(Everywhere.class));
		Assertions.assertNotNull(InstantJaxbAdapter.class.getPackage().getAnnotation(Everywhere.class));
		Assertions.assertNotNull(LocalDateJaxbAdapter.class.getPackage().getAnnotation(Everywhere.class));
		Assertions.assertNotNull(ModelRoot.class.getPackage().getAnnotation(Everywhere.class));
		Assertions.assertNotNull(ModelChild.class.getPackage().getAnnotation(Everywhere.class));
	}

	private static <T> void assertNotNullAnd(T annotation, Consumer<T> assertion) {
		Assertions.assertNotNull(annotation);
		assertion.accept(annotation);
	}
}
