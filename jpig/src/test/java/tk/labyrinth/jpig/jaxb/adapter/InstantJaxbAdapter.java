package tk.labyrinth.jpig.jaxb.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.Instant;

public class InstantJaxbAdapter extends XmlAdapter<Instant, String> {

	@Override
	public Instant marshal(String v) {
		return Instant.parse(v);
	}

	@Override
	public String unmarshal(Instant v) {
		return v.toString();
	}
}
