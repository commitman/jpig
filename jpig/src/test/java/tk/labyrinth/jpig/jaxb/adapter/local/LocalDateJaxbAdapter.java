package tk.labyrinth.jpig.jaxb.adapter.local;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;

public class LocalDateJaxbAdapter extends XmlAdapter<LocalDate, String> {

	@Override
	public LocalDate marshal(String v) {
		return LocalDate.parse(v);
	}

	@Override
	public String unmarshal(LocalDate v) {
		return v.toString();
	}
}
