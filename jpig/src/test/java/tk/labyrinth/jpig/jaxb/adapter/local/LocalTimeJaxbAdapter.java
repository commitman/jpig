package tk.labyrinth.jpig.jaxb.adapter.local;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalTime;

public class LocalTimeJaxbAdapter extends XmlAdapter<LocalTime, String> {

	@Override
	public LocalTime marshal(String v) {
		return LocalTime.parse(v);
	}

	@Override
	public String unmarshal(LocalTime v) {
		return v.toString();
	}
}
