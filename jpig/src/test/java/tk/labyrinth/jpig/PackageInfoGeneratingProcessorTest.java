package tk.labyrinth.jpig;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.Root;
import tk.labyrinth.jaap.context.ProcessingContext;
import tk.labyrinth.jaap.template.element.PackageElementTemplate;
import tk.labyrinth.jaap.template.element.TypeElementTemplate;
import tk.labyrinth.jaap.testing.junit5.annotation.CompilationTarget;
import tk.labyrinth.jaap.testing.junit5.annotation.ExtendWithJaap;
import tk.labyrinth.jpig.model.PackageContext;
import tk.labyrinth.jpig.test.Everywhere;

import javax.annotation.processing.ProcessingEnvironment;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

@ExtendWithJaap
class PackageInfoGeneratingProcessorTest {

	@CompilationTarget(sourceTypes = Everywhere.class)
	@Test
	void testCollectPackages(ProcessingEnvironment processingEnvironment) {
		TypeElementTemplate typeElementTemplate = ProcessingContext.of(processingEnvironment).getTypeElementTemplate(Root.class);
		PackageElementTemplate packageElementTemplate = typeElementTemplate.getPackage();
		PackageInfoGeneratingProcessor processor = new PackageInfoGeneratingProcessor();
		{
			// Tests ordered input of type, package, result is empty as package is present.
			List<PackageContext> contexts = processor.collectPackages(Stream.of(typeElementTemplate, packageElementTemplate),
					Collections.emptyList());
			Assertions.assertNotNull(contexts);
			Assertions.assertEquals(0, contexts.size());
		}
		{
			// Tests ordered input of package, type, result is empty as package is present.
			List<PackageContext> contexts = processor.collectPackages(Stream.of(packageElementTemplate, typeElementTemplate),
					Collections.emptyList());
			Assertions.assertNotNull(contexts);
			Assertions.assertEquals(0, contexts.size());
		}
	}
}
