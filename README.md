# Java package-info Generator

[![MAVEN-CENTRAL](https://img.shields.io/maven-central/v/tk.labyrinth/jpig.svg?label=Maven+Central)](https://mvnrepository.com/artifact/tk.labyrinth/jpig)
[![LICENSE](https://img.shields.io/badge/license-MIT-green.svg?label=License)](LICENSE)

Tiny library for generating package-info with annotations.

## Maven Dependency

```xml
<dependency>
	<groupId>tk.labyrinth</groupId>
	<artifactId>jpig</artifactId>
	<version>0.8.2</version>
</dependency>
```

## Getting Started

See [examples](jpig-examples).

#### Quick Demo

Declaration:
```java
@javax.annotation.Nonnull
@tk.labyrinth.jpig.Propagate(Nonnull.class)
package tk.labyrinth;
```

Result:
```java
@javax.annotation.Nonnull
package tk.labyrinth.jpig.java8;
```
```java
@javax.annotation.processing.Generated("tk.labyrinth.jpig.PackageInfoGeneratingProcessor")
@javax.annotation.Nonnull
package tk.labyrinth.jpig.java9;
```

#### Root package-info

1. Create package-info.java in your top-level named package (the one which has "package [name];" line in it);
2. Add annotations you want to propagate, like @AwesomeCode;
3. Add propagation annotation @Propagate(AwesomeCode.class);
4. (Re)build project;
5. Watch all subpackages of [name] having package-info.java with @AwesomeCode.

#### Propagation Rules

- @Propagate is a repeatable annotation, multiple may be used to define different rules;
- @Propagate#value specifies the annotations to propagate by this rule;
- @Propagate#acceptMasks specifies file masks to be matched by package name to add annotations to it. Default value is {"*"} (matches all);
- @Propagate#rejectMasks specifies file masks to be matched by package name to remove annotations to it. Default value is {} (matches none).

#### Considerations and Limitations

- This library creates package-info.java in generated sources folder, so:
  - If package-info exists in source code, no package-info is generated, so;
    - If you intend to use this library you should locate all package level annotations inside a single root package-info with all propagation rules to avoid generation misbehaviour;
- If there are multiple annotated package-info with the same name within the classpath (defined in multiple jars) only one of them is loaded and other annotations are lost for runtime.
  - Generated package-info may override the ones defined in libraries and cause system malfunction.
    - Viable solution here is thoughtful usage of package names and package level annotations.
